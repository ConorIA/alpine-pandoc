#!/bin/bash

set -e

git clean -fdx
git pull

pandocver=$(curl -s -N https://github.com/jgm/pandoc/releases.atom | grep -m 1 -o "/571770/.*" | sed -e 's/\/571770\/\(.*\)<\/id>/\1/')
citeprocver=$(curl -s -N https://github.com/jgm/citeproc/releases.atom | grep -m 1 -o "/293376323/.*"  | sed -e 's/\/293376323\/\(.*\)<\/id>/\1/')


for dir in "pandoc" "citeproc"; do
  echo "Updating $dir"
  cd "$dir"
  if [ "$dir" == "pandoc" ]; then
    newver=$pandocver
  elif [ "$dir" == "citeproc" ]; then
    newver=$citeprocver
  fi
  oldver=$(grep -a "pkgver=" APKBUILD | sed 's/pkgver=//g')
  echo "Current version number is $newver."
  oldrel=$(grep -a "pkgrel=" APKBUILD | sed 's/pkgrel=//g')
  if [ "$newver" != "$oldver" ]; then
    echo "Updating: $oldver -> $newver"
    sed -i "5s/.*/pkgver=$newver/" APKBUILD
    if [ "$oldrel" != "0" ]; then
      echo "Resetting release to 0."
      sed -i "6s/.*/pkgrel=0/" APKBUILD
    fi
    #FIXME: Try to install pandoc manually to avoid problems with the ver chosen for depends
    if [ "$dir" == "citeproc" ]; then
      sed -i "28s/.*/  cabal install --user pandoc-$pandocver/" APKBUILD
    fi
    git add APKBUILD
    git commit -m "Automated $dir version bump"
  fi
  cd ..
done

# Push changes (if any)
git push

echo "Updates completed."
