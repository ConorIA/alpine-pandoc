# Maintainer: Conor Anderson <conor@conr.ca>
# Based on Arch Linux pandoc-cabal PKGBUILD by: Johann Klähn <kljohann@gmail.com>

pkgname=pandoc
pkgver=2.11.4
pkgrel=0
pkgdesc="Conversion between markup formats"
url="http://pandoc.org"
arch="x86_64"
license="GPL"
depends="gmp libffi zlib pcre cmark"
makedepends="ghc cabal linux-headers musl-dev gmp-dev zlib-dev"
source="http://hackage.haskell.org/packages/archive/${pkgname}/${pkgver}/${pkgname}-${pkgver}.tar.gz"
builddir="$srcdir/$pkgname-$pkgver"


build() {
  cd "${builddir}"

  cabal v1-sandbox init
  export PATH="$PWD/.cabal-sandbox/bin:$PATH"

  cabal v1-update

  echo "Building dependencies..."
  cabal v1-install --user hsb2hs
  cabal v1-install --user --only-dependencies

  echo "Configuring..."
  cabal v1-configure -v \
    --prefix=/usr \
    --libsubdir='$pkg' \
    --datasubdir='$pkg' \
    --docdir='$datadir/doc/$pkg'

  echo "Starting build..."
  cabal v1-build
}

package() {
  cd "${builddir}"

  cabal v1-sandbox init
  export PATH="$PWD/.cabal-sandbox/bin:$PATH"

  cabal v1-copy --destdir="${pkgdir}/"
  rm -r "${pkgdir}/usr/lib"
}

sha512sums="4c18aa3373ec82b47c438bb3382936ac89516c0b9c6bc2e83b613cd2c2f92197e21f36db90a0a4f3f9df88a6de3ee68bbeabfdd4c96e6e18e3e74025c489651c  pandoc-2.9.tar.gz"
