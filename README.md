# alpine-pandoc

A repo that builds ports of pandoc and citeproc for Alpine.

## Usage

```
sudo wget https://gitlab.com/ConorIA/alpine-pandoc/raw/master/conor@conr.ca-584aeee5.rsa.pub -O /etc/apk/keys/conor@conr.ca-584aeee5.rsa.pub
echo https://conoria.gitlab.io/alpine-pandoc/ >> /etc/apk/repositories
echo "@testing http://nl.alpinelinux.org/alpine/edge/testing" >> /etc/apk/repositories
apk add --no-cache cmark@testing pandoc citeproc
```
